# README #

This repository contains an implementation of a command stack for the Graphical Editing Framework (GEF) in Eclipse that supports commands with post conditions. When such a command is put on the command stack, the command is first executed and then the post condition is checked. If the post condition is false, the command stack will undo the command.

The command stack is based on the command stack implementation in GEF, which is licensed under EPL.