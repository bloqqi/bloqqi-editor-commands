/*******************************************************************************
 * Copyright (c) 2013-2017 Niklas Fors
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Niklas Fors
 *******************************************************************************/

package org.bloqqi.editor.commands;

import org.eclipse.gef.commands.Command;

public abstract class PostConditionCommand extends Command {
	public abstract boolean couldExecute();
	public abstract String getErrorMessage();
}
